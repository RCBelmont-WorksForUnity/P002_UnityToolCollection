﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Package
{
    public class FormData : ScriptableObject
    {
        [SerializeField] public float Width = 0;
        [SerializeField] public float Height = 0;

        [SerializeField] public float CellWidth = 0;
        [SerializeField] public float CellHeight = 0;
        
        [SerializeField] public List<Vector3> PosL = new List<Vector3>();

        [SerializeField] public Vector3 LeaderPos = Vector3.zero;
        [SerializeField] public Vector3 CenterPos = Vector3.zero;
        
    }
}