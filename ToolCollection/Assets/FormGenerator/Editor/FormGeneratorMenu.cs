﻿//*****************************************************************************
//Created By ZJ on 2019年2月18日.
//
//@Description 阵型生成器目录脚本
//*****************************************************************************
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Package
{
   
    public class ObjectGeneratorMenu : Editor
    {
        [MenuItem("GameObject/FormGenerator", false, 11)]
        public static void CreateGenerator()
        {
            GameObject obj = new GameObject();
            obj.name = "Generator";
            obj.AddComponent<FormGenerator>();
        }
    }
}

