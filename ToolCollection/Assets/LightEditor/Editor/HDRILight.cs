﻿//*****************************************************************************
//Created By RCBelmont on 2018年6月15日.
//
//@Description 天空盒光源编辑器
//*****************************************************************************

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;


public class HDRILight : EditorWindow
{
    //桥接RenderTextrue，用于将转换后的天空盒贴图转换到可读取的Texture上
    private RenderTexture _rt;
    
    //当前操作状态
    private enum Status
    {
        Idel,   //无操作待机状态
        LeftMouseDown //左键按下状态
    };

    //颜色通道
    private enum ColorChannel
    {
        R,  //红
        G,  //绿
        B   //蓝
    };

    private List<LightInfo> _lightL;//光源信息列表
    private LightInfo _currLightInfo;//当前选择的光源信息
    private Status _status = Status.Idel;//操作状态

    private Color _currColor = Color.gray;//当前采样颜色
    private Material _m;    //绘制图片的材质
    private Texture _lightTexture; //光源图标
    private Texture2D _showTexture; //当前显示的天盒图片

    private bool _showName = false; //是否显示光源名称
    private bool _needUpdate = true; //是否需要更新天盒图片
  
    private GameObject _skyLightGroup; //天盒光源父物体

    public static int W = 600; //图片绘制宽度
    public static int H = W / 2; //图片绘制高度
    private const int MAX_LIGHT_COUNT = 10;//最大光源数量
    private float _baseIntensity = 1; //天空盒亮度



    [MenuItem("Tools/HDRILight")]
    static void AddWindow()
    {
        //创建窗口
        //窗口尺寸
        Rect winRect = new Rect(0, 0, 600, 500);
        HDRILight window =
            (HDRILight) EditorWindow.GetWindowWithRect(typeof(HDRILight), winRect, true, "HDRILight");
        window.Show();
    }

    private void Awake()
    {
        if (_rt == null)
        {
            //获取零时RenderTexture
            _rt = RenderTexture.GetTemporary(600, 300, 0, RenderTextureFormat.DefaultHDR);
        }
        //初始化
        _showTexture = Texture2D.blackTexture;
        _m = new Material(Shader.Find("Hidden/HDRILightEditor"));
        _lightTexture = Resources.Load("_editor_light_icon") as Texture;
        _baseIntensity = RenderSettings.skybox.GetFloat("_Exposure");
        InitLightInfo();
    }

    private void OnDestroy()
    {
        if (_rt != null)
        {
            //释放RenderTex
            RenderTexture.ReleaseTemporary(_rt);
            _rt = null;
        }
    }

    private void OnInspectorUpdate()
    {
        //将天空盒贴图渲染到Texture2D上
        RenderCubeMap();
    }

    private void OnFocus()
    {
        //获取焦点时更新光源信息和天盒信息
        _needUpdate = true;
        InitLightInfo();
    }

    private void OnGUI()
    {
        Rect textureRect = new Rect(0, 0, W, H);
        //绘制天盒图片
        Graphics.DrawTexture(textureRect, _showTexture);
        #region 操作监听
        Event current = Event.current;
        int controlID = GUIUtility.GetControlID(FocusType.Passive);
        EventType typeForControl = current.GetTypeForControl(controlID);
        switch (typeForControl)
        {
            case EventType.MouseDown:
                if (current.button == 0 && textureRect.Contains(current.mousePosition))
                {
                    _status = Status.LeftMouseDown;
                    _currColor = GetPixelColor(_showTexture, current.mousePosition);
                    _currLightInfo = MouseOnLight(current.mousePosition);
                    if (_currLightInfo == null)
                    {
                        if (_lightL.Count < MAX_LIGHT_COUNT)
                        {
                            _currLightInfo = new LightInfo(current.mousePosition, _currColor, _skyLightGroup);
                            _lightL.Add(_currLightInfo);
                            EditorSceneManager.MarkAllScenesDirty();
                            Repaint();
                        }
                    }

                    if (_currLightInfo != null)
                    {
                        Selection.activeObject = _currLightInfo.LightObj;
                    }
                }

                if (current.button == 1)
                {
                    LightInfo mouseOn = MouseOnLight(current.mousePosition);
                    if (mouseOn != null && _status != Status.LeftMouseDown)
                    {
                        _currLightInfo = null;
                        mouseOn.Destroy();
                        _lightL.Remove(mouseOn);
                        EditorSceneManager.MarkAllScenesDirty();
                        Repaint();
                    }
                }

                break;
            case EventType.MouseUp:
                if (current.button == 0)
                {
                    _status = Status.Idel;
                    _currLightInfo = null;
                }

                break;
            case EventType.MouseDrag:
                if (current.button == 0 && textureRect.Contains(current.mousePosition))
                {
                    _currColor = GetPixelColor(_showTexture, current.mousePosition);
                    if (_currLightInfo != null)
                    {
                        _currLightInfo.UpdateLightInfo(current.mousePosition, _currColor);
                    }

                    EditorSceneManager.MarkAllScenesDirty();
                }

                break;
        }

        #endregion
        //绘制剩余GUI
        #region 绘制剩余GUI
        switch (_status)
        {
            case Status.LeftMouseDown:
                EditorGUI.ColorField(new Rect(10, 320, 100, 30), _currColor);
                EditorGUI.LabelField(new Rect(10, 360, 50, 20), "R:" + _currColor.r);
                EditorGUI.LabelField(new Rect(10, 390, 50, 20), "G:" + _currColor.g);
                EditorGUI.LabelField(new Rect(10, 420, 50, 20), "B:" + _currColor.b);
                if (_currLightInfo != null)
                {
                    GUI.Label(new Rect(220, 320, 200, 20), "当前光源：" + _currLightInfo.LightObj.name);
                }

                Repaint();
                break;
        }

        GUI.Label(new Rect(425, 320, 200, 20), "光源数量：" + _skyLightGroup.transform.childCount + "/" + MAX_LIGHT_COUNT);
        if (GUI.Button(new Rect(220, 360, 150, 60), "清除所有光源"))
        {
            ClearAllLight();
            Repaint();
        }

        if (GUI.Button(new Rect(380, 360, 150, 60), "刷新所有光源"))
        {
            UpdateLight(_showTexture);
            Repaint();
        }

        GUI.Label(new Rect(150, 450, 200, 15), "背景强度:");
        if (_lightL == null)
        {
            InitLightInfo();
        }

        GUI.Label(new Rect(450, 450, 100, 15), "显示光源名称");
        _showName = EditorGUI.Toggle(new Rect(520, 450, 100, 100), _showName);
        GUIStyle guiStyle = new GUIStyle();
        guiStyle.normal.textColor = Color.white;
        foreach (LightInfo lightInfo in _lightL)
        {
            _m.SetTexture("_MainTex1", _lightTexture);
            _m.SetColor("_ColorTint", lightInfo.LightColor * lightInfo.LightIns);
            Graphics.DrawTexture(lightInfo.Rect, Texture2D.blackTexture, _m, 1);
            if (_showName)
            {
              
                GUI.Box(new Rect(lightInfo.Rect.x + lightInfo.Rect.width / 2 - 50, lightInfo.Rect.y + lightInfo.Rect.height + 5, 100, 20), lightInfo.LightObj.name);

            }

            //EditorSceneManager.MarkAllScenesDirty();
        }

        EditorGUI.BeginChangeCheck();
        _baseIntensity = EditorGUI.Slider(new Rect(220, 450, 200, 15), "", RenderSettings.skybox.GetFloat("_Exposure"),
            0, 8);
#endregion
        if (EditorGUI.EndChangeCheck())
        {
            RenderCubeMap(true);
            RenderSettings.skybox.SetFloat("_Exposure", _baseIntensity);
            UpdateLight(_showTexture);
        }

    }

    #region 内部方法
    //获取像素颜色
    Color GetPixelColor(Texture2D t, Vector2 pos)
    {
        return t.GetPixel(Mathf.FloorToInt(pos.x),
            Mathf.FloorToInt(t.height - pos.y));
    }
    //将RenderTexture数据拷贝到Texture2D上
    Texture2D CopyRrenderTextureToTexture2D(RenderTexture rt)
    {
        RenderTexture currentActive = RenderTexture.active;
        RenderTexture.active = rt;
        Texture2D ret = new Texture2D(rt.width, rt.height, TextureFormat.RGBAFloat, false);
        ret.ReadPixels(new Rect(0, 0, ret.width, ret.height), 0, 0);
        ret.Apply();
        RenderTexture.active = currentActive;
        return ret;
    }
    //判断鼠标是否点击在光源图标上
    LightInfo MouseOnLight(Vector2 pos)
    {
        if (_lightL.Count <= 0)
        {
            return null;
        }

        for (int i = 0; i < _lightL.Count; i++)
        {
            if (_lightL[i].CheckInRect(pos))
            {
                return _lightL[i];
            }
        }

        return null;
    }
    //初始化光源信息
    void InitLightInfo()
    {
        _skyLightGroup = GameObject.Find("SkyLight");
        if (_skyLightGroup == null)
        {
            _skyLightGroup = new GameObject();
            _skyLightGroup.name = "SkyLight";
        }

        _lightL = new List<LightInfo>();
        if (_skyLightGroup == null)
        {
            return;
        }

        for (int i = 0; i < _skyLightGroup.transform.childCount; i++)
        {
            GameObject temp = _skyLightGroup.transform.GetChild(i).gameObject;
            if (temp.activeSelf)
            {
                Light tempLight = temp.GetComponent<Light>();
                if (tempLight != null && tempLight.type == LightType.Directional)
                {
                    Vector3 dir = -temp.transform.forward;
                    float alpha = Vector3.Angle(new Vector3(dir.x, 0, dir.z), Vector3.forward) *
                                  (Vector3.Dot(dir, Vector3.right) > 0 ? 1 : -1);
                    float beta = Vector3.Angle(dir, Vector3.up);
                    Vector2 pos = new Vector2(Mathf.Lerp(0, W, (alpha + 180) / 360), Mathf.Lerp(0, H, beta / 180));
                    LightInfo tempLightInfo = new LightInfo(pos, temp);
                    _lightL.Add(tempLightInfo);
                }
            }
        }
    }
    //将天空盒贴图绘制到Texture2D上
    private void RenderCubeMap()
    {
        RenderCubeMap(false);
    }
    //将天空盒贴图绘制到Texture2D上
    private void RenderCubeMap(bool force)
    {
        Color skyTint = RenderSettings.skybox.GetColor("_Tint");
        _m.SetColor("_ColorTint", skyTint);
        if (RenderSettings.skybox.HasProperty("_Tex"))
        {
            Cubemap cube = (Cubemap) RenderSettings.skybox.GetTexture("_Tex");

            if (_needUpdate || force)
            {
                _m.SetVector("_Size", new Vector4(1, 1, 0, 0));
                _m.SetFloat("_Intensity", _baseIntensity);
                _rt.DiscardContents();
                Graphics.Blit(cube, _rt, _m, 0);
                Repaint();
            }
        }
        else if (RenderSettings.skybox.HasProperty("_FrontTex"))
        {
            if (_needUpdate || force)
            {
                _m.SetTexture("_FrontTex", RenderSettings.skybox.GetTexture("_FrontTex"));
                _m.SetTexture("_BackTex", RenderSettings.skybox.GetTexture("_BackTex"));
                _m.SetTexture("_RightTex", RenderSettings.skybox.GetTexture("_RightTex"));
                _m.SetTexture("_LeftTex", RenderSettings.skybox.GetTexture("_LeftTex"));
                _m.SetTexture("_UpTex", RenderSettings.skybox.GetTexture("_UpTex"));
                _m.SetTexture("_DownTex", RenderSettings.skybox.GetTexture("_DownTex"));
                _m.SetVector("_Size", new Vector4(1, 1, 0, 0));
                _m.SetFloat("_Intensity", _baseIntensity);
                Graphics.Blit(Texture2D.blackTexture, _rt, _m, 2);
            }
        }

        _showTexture = CopyRrenderTextureToTexture2D(_rt);
        _needUpdate = false;
    }
    //更新光源信息
    private void UpdateLight(Texture2D t)
    {
        foreach (LightInfo lightInfo in _lightL)
        {
            lightInfo.UpdateLightInfo(lightInfo.PixelPos, GetPixelColor(t, lightInfo.PixelPos));
        }
    }
    //清理所有光源
    private void ClearAllLight()
    {
        foreach (LightInfo lightInfo in _lightL)
        {
            DestroyImmediate(lightInfo.LightObj);
        }

        _lightL = new List<LightInfo>();
    }

    #endregion

    #region 光源信息类

    class LightInfo
    {
        //光源图标绘制尺寸
        private float SIZE = 30;
        private Rect _rect;
        public Rect Rect
        {
            get
            {
                return _rect;
            }
        }
        //光源对应像素坐标
        public Vector2 PixelPos
        {
            get
            {
               return new Vector2(_rect.x + _rect.width / 2, _rect.y + _rect.height / 2);
            }
        }
        //光源对应GameObject
        private GameObject _lightObj;
        public GameObject LightObj
        {
            get { return _lightObj; }
        }
        //光源的Light Component
        private Light _light;
        //光源颜色
        public Color LightColor
        {
            get
            {
                if (_light) return _light.color;
                return Color.black;
            }
        }
        //光源强度
        public float LightIns
        {
            get
            {
                if (_light) return _light.intensity;
                return 0;
            }
        }
        //构造函数
        public LightInfo(Vector2 pos, GameObject light)
        {
            _lightObj = light;
            _light = _lightObj.GetComponent<Light>();
            UpdatePos(pos);
        }
        //构造函数
        public LightInfo(Vector2 pos, Color color, GameObject parent)
        {
            _lightObj = new GameObject();
            int lastIdx = 0;
            int slot = 0;
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                GameObject o = parent.transform.GetChild(i).gameObject;
                string[] sl = o.name.Split(new char[] {'_'}, 2);
                int idx = 0;
                if (sl.Length == 2 && IsNumber(sl[1], out idx))
                {
                    slot |= 1 << idx;
                }
            }
            while (slot > 0)
            {
                int tick = slot & 1;
                if (tick == 0)
                {
                    break;
                }

                lastIdx++;
                slot = slot >> 1;
            }
            _lightObj.name = "skyLight_" + lastIdx;
            _lightObj.transform.SetParent(parent.transform, true);
            _light = _lightObj.AddComponent<Light>();
            _light.type = LightType.Directional;
            _light.shadows = LightShadows.None;
            UpdateLightInfo(pos, color);
        }
        //更新光源信息
        public void UpdateLightInfo(Vector2 pos, Color color)
        {
            Color trueColor = new Color(0, 0, 0);
            float intensity = 0;
            switch (GetBrightChennel(color))
            {
                case ColorChannel.R:
                    trueColor.r = 1;
                    intensity = color.r / trueColor.r;
                    trueColor.g = intensity == 0 ? 0 : color.g / intensity;
                    trueColor.b = intensity == 0 ? 0 : color.b / intensity;
                    break;
                case ColorChannel.G:
                    trueColor.g = 1;
                    intensity = color.g / trueColor.g;
                    trueColor.r = intensity == 0 ? 0 : color.r / intensity;
                    trueColor.b = intensity == 0 ? 0 : color.b / intensity;
                    break;
                default:
                    trueColor.b = 1;
                    intensity = color.b / trueColor.b;
                    trueColor.g = intensity == 0 ? 0 : color.g / intensity;
                    trueColor.r = intensity == 0 ? 0 : color.r / intensity;
                    break;
            }

            _light.color = trueColor;
            _light.intensity = intensity;
            UpdatePos(pos);
        }

        //检查某点是否在绘制区域内
        public bool CheckInRect(Vector2 pos)
        {
            return _rect.Contains(pos);
        }
        //销毁GameObject
        public void Destroy()
        {
            DestroyImmediate(_lightObj);
        }
        //更新位置
        private void UpdatePos(Vector2 pos)
        {
            _rect = new Rect(pos.x - SIZE / 2, pos.y - SIZE / 2, SIZE, SIZE);
            float alpha = pos.x / HDRILight.W * 2 - 1;
            float beta = pos.y / HDRILight.H * 2 - 1;
            Vector3 dir = new Vector3(Mathf.Sin(alpha * Mathf.PI), Mathf.Tan(-beta * Mathf.PI / 2),
                Mathf.Cos(alpha * Mathf.PI));
            _lightObj.gameObject.transform.LookAt(_lightObj.transform.position - dir.normalized);
        }
        //提取最亮的通道
        private ColorChannel GetBrightChennel(Color c)
        {
            if (c.r >= c.b && c.r >= c.g)
            {
                return ColorChannel.R;
            }

            if (c.b >= c.r && c.b >= c.g)
            {
                return ColorChannel.B;
            }

            return ColorChannel.G;
        }
        //判断是否为数字
        private bool IsNumber(string s, out int result)
        {
            result = -1;
            try
            {
                result = Convert.ToInt32(s);
                return true;
            }
            catch 
            {
                return false; 
            }
        }
    }

    #endregion
}