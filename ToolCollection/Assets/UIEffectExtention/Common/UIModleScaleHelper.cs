﻿//*****************************************************************************
//Created By ZJ on 2018年12月4日.
//
//@Description DESC
//*****************************************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JZYX.UIEffectExtentions
{
    [RequireComponent(typeof(RectTransform))]
    [DisallowMultipleComponent]
    [ExecuteInEditMode]
    public class UIModleScaleHelper : UIBehaviour
    {
       public enum Fit
        {
            height,
            width,
            best
        }

        public Fit fit = Fit.best;
        public Bounds bounds = new Bounds(Vector3.zero, Vector3.one * 50);
        public RectTransform scaler;
        private Vector2 scaleFactor, rectSize;
        private Vector3[] cornersArray = new Vector3[4];
        private RectTransform _rectTrans;

        public Vector3 GetTargetScale(float scaleFactor = 1f)
        {
            return Vector3.one * FittingScaleFactor() * scaleFactor;
        }
        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();
           
            ScaleRenderer();

        }
        public void ScaleRenderer(float scaleFactor = 1f)
        {
            if (scaler)
            {
                scaler.localScale = GetTargetScale(scaleFactor);
            }
           
        }
        public float FittingScaleFactor()
        {
            Vector2 scaleFactor = CalculateScaleFactor();
            float factor = 1f;
            switch (fit)
            {
                case Fit.height:
                    factor = scaleFactor.y;
                    break;
                case Fit.width:
                    factor = scaleFactor.x;
                    break;
                case Fit.best:
                    factor = Mathf.Min(scaleFactor.x, scaleFactor.y);
                    break;
            }

            //When called at Awake, the Rect Transform Bounds are not properly initialized resulting 
            //in NaN Scale at this point. This is mostly a problem with layouted elements so consider
            //using the ForceLayoutInitialization script with layout groups
            if (float.IsNaN(factor) || float.IsInfinity(factor))
                return 1f;
            //the factor should also never become zero
            else if (FloatNullCheck(factor))
                return 0.01f;
            else
                return
                    factor;
        }
        bool FloatNullCheck(float f)
        {
            return Mathf.Approximately(f, 0f);
        }

        public Vector2 CalculateScaleFactor()
        {
            Vector3 boundSize = bounds.size;
            Vector2 desiredBoundSize = GetRectSize();
            scaleFactor.x = desiredBoundSize.x / boundSize.x;
            scaleFactor.y = desiredBoundSize.y / boundSize.y;
            return scaleFactor;
        }
        public Vector2 GetRectSize()
        {
            RectTrans.GetWorldCorners(cornersArray);
            rectSize.x = Mathf.Abs(cornersArray[0].x - cornersArray[2].x);
            rectSize.y = Mathf.Abs(cornersArray[0].y - cornersArray[2].y);
            return rectSize;
        }

        public RectTransform RectTrans
        {
            get
            {
                if (_rectTrans == null)
                    _rectTrans = GetComponent<RectTransform>();
                return _rectTrans;
            }
        }
    }

    
}


