﻿//*****************************************************************************
//Created By ZJ on 2018年12月5日.
//
//@Description 模型缩放器Editor代码
//*****************************************************************************
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace JZYX.UIEffectExtentions
{
    [CustomEditor(typeof(UIModleScaleHelper))]

    public class UIModleScaleHelperEditor : Editor
    {
        private Color color = Color.white;
#if UNITY_2017_1_OR_NEWER
        private BoxBoundsHandle m_BoundsHandle = new BoxBoundsHandle();
#else
		private BoxBoundsHandle m_BoundsHandle = new BoxBoundsHandle(typeof(CanvasBoundsElementEditor).GetHashCode());
#endif
        void OnSceneGUI()
        {
            UIModleScaleHelper script = (UIModleScaleHelper) target;

            // copy the target object's data to the handle
            m_BoundsHandle.center = script.transform.position + script.bounds.center;
            m_BoundsHandle.size = script.bounds.size;
            m_BoundsHandle.wireframeColor = color;

            // draw the handle
            EditorGUI.BeginChangeCheck();
            m_BoundsHandle.DrawHandle();
            if (EditorGUI.EndChangeCheck())
            {
                // record the target object before setting new values so changes can be undone/redone
                Undo.RecordObject(script, "Change Bounds of CanvasBoundsElement");

                // copy the handle's updated data back to the target object
                Bounds newBounds = new Bounds();
                newBounds.center = m_BoundsHandle.center - script.transform.position;
                newBounds.size = m_BoundsHandle.size;
                script.bounds = newBounds;
            }
        }
    }
}