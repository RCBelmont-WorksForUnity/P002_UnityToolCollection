﻿//*****************************************************************************
//Created By ZJ on 2018年11月23日.
//
//@Description UI自定义Mesh界面代码
//*****************************************************************************
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

namespace JZYX.UIEffectExtentions
{
    [CustomEditor(typeof(UICustomMesh))]
    [CanEditMultipleObjects]
    public class UICustomMeshEditor : GraphicEditor
    {

        static readonly GUIContent _meshCotnent = new GUIContent("Mesh", "渲染目标的mesh");
        static readonly GUIContent _raycastTargetContent = new GUIContent("RaycastTarget", "RaycastTarget");
        static readonly GUIContent _targetC = new GUIContent("TargetObj", "要渲染的GambeObject");
        static readonly GUIContent _mrC = new GUIContent("MeshRenderer", "目标对象的MeshRenderer");
        static readonly GUIContent _mfC = new GUIContent("MeshFilter", "目标对象的MeshFilter");
        static readonly GUIContent _smrC = new GUIContent("SkinnedMeshRenderer", "目标对象的SkinnedMeshRenderer");
       

        private SerializedProperty _target;
        private SerializedProperty _mr;
        private SerializedProperty _mf;
        private SerializedProperty _smr;
        private SerializedProperty _mesh;
        private SerializedProperty _raycastTarget;
      
    

        protected override void OnEnable()
        {
            base.OnEnable();
            _mesh = serializedObject.FindProperty("_mesh");
            _target = serializedObject.FindProperty("_target");
            _mr = serializedObject.FindProperty("_mr");
            _mf = serializedObject.FindProperty("_mf");
            _smr = serializedObject.FindProperty("_smr");
            _raycastTarget = serializedObject.FindProperty("m_RaycastTarget");
          
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_target, _targetC);
            EditorGUILayout.PropertyField(_raycastTarget, _raycastTargetContent);
           
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(_mesh, _meshCotnent);
            EditorGUILayout.PropertyField(_mr, _mrC);
            EditorGUILayout.PropertyField(_mf, _mfC);
            EditorGUILayout.PropertyField(_smr, _smrC);
            EditorGUI.EndDisabledGroup();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
