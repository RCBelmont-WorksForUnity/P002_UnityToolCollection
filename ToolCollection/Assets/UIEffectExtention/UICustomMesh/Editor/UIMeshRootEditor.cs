﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace JZYX.UIEffectExtentions
{
    [CustomEditor(typeof(UIMeshRoot))]
    public class UIMeshRootEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            UIMeshRoot t = (UIMeshRoot) target;
            t.rayCastTarget = EditorGUILayout.Toggle("RayCastTarget", t.rayCastTarget);
            if (GUILayout.Button("设置CanvasRenderer"))
            {
                t.SetRenderer();
            }
        }
    }
}