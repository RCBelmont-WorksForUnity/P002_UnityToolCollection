﻿//*****************************************************************************
//Created By ZJ on 2018年12月4日.
//
//@Description UI渲染Mesh的根节点, 用于批量处理子节点的Renderer替换以及缩放处理
//*****************************************************************************

using System;
using UnityEngine;

namespace JZYX.UIEffectExtentions
{
    public class UIMeshRoot : MonoBehaviour
    {
        public bool rayCastTarget = false;
        // Use this for initialization
        void Start()
        {
            SetRenderer();
        }

        /// <summary>
        /// 替换渲染的GameObj
        /// </summary>
        /// <param name="obj"></param>
        public void ChangeModel(GameObject obj)
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                Destroy(this.transform.GetChild(i));
            }

            obj.transform.SetParent(this.transform, false);
            SetRenderer();
        }

        /// <summary>
        /// 将原有的Renderer替换为CanvasRenderer
        /// </summary>
        public void SetRenderer()
        {
            foreach (Renderer r in this.transform.GetComponentsInChildren<Renderer>())
            {
                Type rType = r.GetType();
                if (rType == typeof(MeshRenderer) || rType == typeof(SkinnedMeshRenderer))
                {
                    UICustomMesh t = r.GetComponent<UICustomMesh>();
                    if (t == null)
                    {
                        t = r.gameObject.AddComponent<UICustomMesh>();
                    }
                    t.Init(rayCastTarget);
                }
            }
        }
    }
}