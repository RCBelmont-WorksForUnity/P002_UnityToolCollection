﻿//*****************************************************************************
//Created By ZJ on 2018年11月23日.
//
//@Description UI粒子界面代码
//*****************************************************************************
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

namespace JZYX.UIEffectExtentions
{
    [CustomEditor(typeof(UIParticle))]
    [CanEditMultipleObjects]
    public class UIParticleEditor : GraphicEditor
    {
        static readonly GUIContent _particleMatContent = new GUIContent("Particle Material", "The material for rendering particles");
        static readonly GUIContent _trailMatContent = new GUIContent("Trail Material", "The material for rendering particle trails");
        static readonly GUIContent _raycastTargetContent = new GUIContent("RaycastTarget", "RaycastTarget");

        SerializedProperty _particleMat;
        SerializedProperty _trailMat;
        SerializedProperty _raycastTarget;
        protected override void OnEnable()
        {
            base.OnEnable();
            _particleMat = serializedObject.FindProperty("_material");
            _trailMat = serializedObject.FindProperty("_traMaterial");
            _raycastTarget = serializedObject.FindProperty("m_RaycastTarget");
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(_particleMat, _particleMatContent);
            EditorGUILayout.PropertyField(_trailMat, _trailMatContent);
            EditorGUILayout.PropertyField(_raycastTarget, _raycastTargetContent);
            serializedObject.ApplyModifiedProperties();
        }
    }
}