﻿//*****************************************************************************
//Created By ZJ on 2018年11月30日.
//
//@Description UI帧动画管理器
//*****************************************************************************

using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace JZYX.UIEffectExtentions
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Image))]
    public class UIFrameAnimator : MonoBehaviour
    {
        //播放状态
        enum PlayStatus
        {
            Stop,
            Playing,
            Pause
        }

        private Image _img;//用以显示帧动画的image组件
        private Material _mat;//image组件使用的材质
        [SerializeField] private Texture2D _mainTex;//序列帧图集纹理
        [SerializeField] private Color _color = Color.white;//颜色偏移
        [SerializeField] private int _hCount = 1;//图集横向帧数
        [SerializeField] private int _vCount = 1;//图集纵向帧数
        [Range(1, 100)] [SerializeField] private float _speed = 1;//帧率
        [SerializeField] private bool _playOnAwake = true;//是否开始就播放
        [SerializeField] private int _loopCount = 0;//循环数量, 0为无限循环
        [SerializeField] private float _loopInterval = 0;//循环间隔(S)
        [SerializeField] private bool _stopOnLastFrame = true;//循环结束
        [SerializeField] private CullMode _cullMode = CullMode.Back;//剔除模式
        [SerializeField] private BlendMode _sBlend = BlendMode.SrcAlpha;//源混合参数
        [SerializeField] private BlendMode _dBlend = BlendMode.OneMinusSrcAlpha;//目标混合参数
        private PlayStatus _playStatus = PlayStatus.Stop;//播放状态

        //Shader属性ID缓存便于随时调用
        private int _colorID = Shader.PropertyToID("_Color");
        private int _mainTexID = Shader.PropertyToID("_MainTex");
        private int _hCountID = Shader.PropertyToID("_HorizontalAmount");
        private int _vCountID = Shader.PropertyToID("_VerticalAmount");
        private int _speedID = Shader.PropertyToID("_Speed");
        private int _TimeClockID = Shader.PropertyToID("_TimeClock");
        private int _cullID = Shader.PropertyToID("_Cull");
        private int _sourceBlendID = Shader.PropertyToID("_SourceBlend");
        private int _destBlendID = Shader.PropertyToID("_DestBlend");

        private float _time0;//上一帧时间
        private float _time;//
        private int _loopC;
        private float _intervalTime = 0;


        void OnEnable()
        {
            _img = this.gameObject.GetComponent<Image>();
            if (_img)
            {
                //TODO:根据情况可修改为支持热更的Shader加载方式
                _mat = new Material(Shader.Find("UIExtention/SequenceFrameAnimation"));
                _mat.hideFlags = HideFlags.NotEditable | HideFlags.DontSave;
                _img.material = _mat;
                UpdateMat();
            }
        }

        void Start()
        {
            if (_playOnAwake)
            {
                Play();
            }
        }

        void OnDisable()
        {
            if (_mat)
            {
                DestroyImmediate(_mat);
            }
        }

        public bool isPlaying
        {
            get { return _playStatus == PlayStatus.Playing; }
        }

        //播放结束回调(非无限循环情况下, 播放结束触发的回调)
        public delegate void PlayEndHadler();

        public Action OnPlayEnd;

        public void AddPlayEndCb(Action cb)
        {
            OnPlayEnd = cb;
        }

        public void Play()
        {
            if (_playStatus == PlayStatus.Pause)
            {
                //_startTime = Time.realtimeSinceStartup;
                _loopC = 0;
                _playStatus = PlayStatus.Playing;
            }
            else if (_playStatus != PlayStatus.Playing)
            {
                _loopC = 0;
                _time0 = Time.realtimeSinceStartup;
                _playStatus = PlayStatus.Playing;
            }
        }

        public void Stop(bool lastframe = false)
        {
            if (_playStatus != PlayStatus.Stop)
            {
                _time = 0;
                _time0 = 0;
                _playStatus = PlayStatus.Stop;
                SetFrame(lastframe);
            }
        }

        public void Pause()
        {
            if (_playStatus == PlayStatus.Playing)
            {
                _playStatus = PlayStatus.Pause;
            }
        }

        private void Update()
        {
            if (Application.isPlaying)
            {
                UpdateFrame();
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (!Application.isPlaying)
            {
                UpdateFrame();
            }
        }
        //帧更新
        private void UpdateFrame()
        {
            //获取真实事件变化,并应用当前的TimeScale
            float deltaTime = (Time.realtimeSinceStartup - _time0) * Time.timeScale;
            if (_mat)
            {
                if (_playStatus == PlayStatus.Playing)
                {
                    float nowTime = _time + deltaTime;

                    float loopTime = ((float) _hCount * (float) _vCount) / _speed;
                    int nowLoop = Mathf.FloorToInt(nowTime / loopTime);
                    //当前的循环数大于等于指定的循环数, 则停止播放并调用回调
                    if (_loopCount > 0 && nowLoop >= _loopCount)
                    {
                        Stop(_stopOnLastFrame);
                        if (OnPlayEnd != null)
                        {
                            OnPlayEnd();
                        }

                        return;
                    }
                    //处理循环间隔
                    if (nowLoop > _loopC)
                    {
                        if (_loopInterval <= 0)
                        {
                            _loopC = nowLoop;
                            _time = nowTime;
                        }
                        else
                        {
                            if (_intervalTime < _loopInterval)
                            {
                                _intervalTime += deltaTime;
                            }
                            else
                            {
                                _intervalTime = 0;
                                _loopC = nowLoop;
                                _time = nowTime;
                            }
                        }
                    }
                    else
                    {
                        _time = nowTime;
                    }

                    _mat.SetFloat(_TimeClockID, _time);
                }
            }

            _time0 = Time.realtimeSinceStartup;
        }
        //设置当前帧, 支持设置到第一帧和最后一帧
        private void SetFrame(bool last)
        {
            if (_mat)
            {
                if (last)
                {
                    _mat.SetFloat(_TimeClockID, ((float) _hCount * (float) _vCount - 1) / _speed);
                }
                else
                {
                    _mat.SetFloat(_TimeClockID, 0);
                }
            }
        }

        //更新材质
        public void UpdateMat()
        {
            if (_mat)
            {
                _mat.SetTexture(_mainTexID, _mainTex);
                _mat.SetColor(_colorID, _color);
                _mat.SetInt(_hCountID, _hCount);
                _mat.SetInt(_vCountID, _vCount);
                _mat.SetFloat(_speedID, _speed);
                _mat.SetFloat(_cullID, (int)_cullMode);
                _mat.SetFloat(_sourceBlendID, (int)_sBlend);
                _mat.SetFloat(_destBlendID, (int)_dBlend);
                _img.canvasRenderer.SetMaterial(_mat, _mainTex);
            }
        }
    }
}