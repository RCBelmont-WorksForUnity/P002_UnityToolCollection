﻿// 帧动画播放shader
// Description：支持配置帧动画贴图行列、动画速度、叠加颜色
// By Aaron 2018/03/28
Shader "UIExtention/SequenceFrameAnimation"
{
    Properties
    {
        _Color ("Color Tint", Color) = (1, 1, 1, 1) // 主纹理叠加颜色
        [PerRendererData]_MainTex ("Image Sequence", 2D) = "white" { }// 主纹理贴图
        _HorizontalAmount ("Horizontal Amount", Float) = 4 // 帧动画横向数量
        _VerticalAmount ("Vertical Amount", Float) = 4 // 帧动画纵向数量
        _Speed ("Speed", Range(1, 100)) = 30 // 速度
        [Enum(UnityEngine.Rendering.CullMode)]_Cull ("CullMode", Float) = 2

        [Enum(UnityEngine.Rendering.BlendMode)] _SourceBlend ("Source Blend Mode", Float) = 5
        [Enum(UnityEngine.Rendering.BlendMode)] _DestBlend ("Dest Blend Mode", Float) = 10

        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        
        _ColorMask ("Color Mask", Float) = 15
        
        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
        
        Cull [_Cull]
        ColorMask [_ColorMask]
        ZTest [unity_GUIZTestMode]
        Lighting Off ZWrite Off
        Blend[_SourceBlend][_DestBlend]
        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }
        // 前向基础渲染通道
        Pass
        {
            
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "UnityUI.cginc"
            #pragma multi_compile __ UNITY_UI_CLIP_RECT
           
            
            fixed4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _HorizontalAmount;
            float _VerticalAmount;
            float _Speed;
            float _TimeClock;
            float4 _ClipRect;
            
            struct a2v
            {
                float4 vertex: POSITION;
                float2 texcoord: TEXCOORD0;
            };
            
            
            struct v2f
            {
                float4 pos: SV_POSITION;
                float2 uv: TEXCOORD0;
                float4 worldPosition: TEXCOORD1;
            };
            
            
            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.worldPosition = v.vertex;
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {


                // float time = floor(_Time.y * _Speed);
                // float row = floor(time / _HorizontalAmount);
                // float column = time - row * _HorizontalAmount;
                
                float totalFrame = (_TimeClock * _Speed);
                int remainder = totalFrame % (_HorizontalAmount * _VerticalAmount);
                int row = floor(remainder / _HorizontalAmount);
                int column = (remainder % _HorizontalAmount);
                
                half2 uv = half2(i.uv.x / _HorizontalAmount, i.uv.y / _VerticalAmount);
                uv += half2(column / _HorizontalAmount, (_VerticalAmount - 1) / _VerticalAmount - row / _VerticalAmount);
                fixed4 c = tex2D(_MainTex, uv);
                c.rgba *= _Color;

                #ifdef UNITY_UI_CLIP_RECT
                    c.a *= UnityGet2DClipping(i.worldPosition.xy, _ClipRect);
                #endif

                return c;
            }
            
            ENDCG
            
        }
    }
    FallBack "Transparent/VertexLit"
}
