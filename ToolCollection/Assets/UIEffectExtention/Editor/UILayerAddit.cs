﻿//*****************************************************************************
//Created By ZJ on 2018年11月29日.
//
//@Description 导入包时自动增加Layer
//*****************************************************************************
using UnityEditor;
using UnityEngine;

public class UILayerAddit : AssetPostprocessor
{
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
        string[] movedFromAssetPaths)
    {
        foreach (string s in importedAssets)
        {
            if (s.Contains("UILayerAddit"))
            {
                //增加一个叫ruoruo的layer
                AddLayer("UIModel");
                return;
            }
        }
    }

    static void AddTag(string tag)
    {
        if (!isHasTag(tag))
        {
            SerializedObject tagManager =
                new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
            SerializedProperty it = tagManager.GetIterator();
            while (it.NextVisible(true))
            {
                if (it.name == "tags")
                {
                    for (int i = 0; i < it.arraySize; i++)
                    {
                        SerializedProperty dataPoint = it.GetArrayElementAtIndex(i);
                        if (string.IsNullOrEmpty(dataPoint.stringValue))
                        {
                            dataPoint.stringValue = tag;
                            tagManager.ApplyModifiedProperties();
                            return;
                        }
                    }
                }
            }
        }
    }

    static void AddLayer(string layer)
    {
        if (!isHasLayer(layer))
        {
            SerializedObject tagManager =
                new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
            SerializedProperty it = tagManager.GetIterator();
            SerializedProperty layersProp = tagManager.FindProperty("layers");
            int size = layersProp.arraySize;
            for (int i = 8; i < size; i++)
            {
                SerializedProperty l = layersProp.GetArrayElementAtIndex(i);
                if (string.IsNullOrEmpty(l.stringValue))
                {
                    l.stringValue = layer;
                    break;
                }
            }

            tagManager.ApplyModifiedProperties();
        }
    }

    static bool isHasTag(string tag)
    {
        for (int i = 0; i < UnityEditorInternal.InternalEditorUtility.tags.Length; i++)
        {
            if (UnityEditorInternal.InternalEditorUtility.tags[i].Contains(tag))
                return true;
        }

        return false;
    }

    static bool isHasLayer(string layer)
    {
        for (int i = 0; i < UnityEditorInternal.InternalEditorUtility.layers.Length; i++)
        {
            if (UnityEditorInternal.InternalEditorUtility.layers[i].Contains(layer))
                return true;
        }

        return false;
    }
}