﻿//*****************************************************************************
//Created By ZJ on 2018/6/1.
//
//@Description 相机移动操作脚本配套的Editor优化代码
//*****************************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(CameraMoveAdv))]
public class CameraMoveDevEditor : Editor
{
    private string[] aixs = new string[]
    {
        "世界坐标X轴",
        "世界坐标Y轴",
        "世界坐标Z轴",
        "本地坐标X轴",
        "本地坐标Y轴",
        "本地坐标Z轴"
    };
    public override void OnInspectorGUI()
    {
        CameraMoveAdv t = (CameraMoveAdv)target;
        EditorGUI.BeginChangeCheck();
        t.AllowTouchX = EditorGUILayout.Toggle("允许左右拖动操作相机", t.AllowTouchX);
        t.AllowTouchY = EditorGUILayout.Toggle("允许上下拖动操作相机", t.AllowTouchY);

        if (t.CamMoveType != CameraMoveAdv.MoveType.move ||
            ((t.TouchX != CameraMoveAdv.Axis.localZ && t.TouchY != CameraMoveAdv.Axis.localZ)))
        {
            t.AllowZoom = EditorGUILayout.Toggle("允许缩放操作相机", t.AllowZoom);
        }

        t.CamMoveType = (CameraMoveAdv.MoveType)EditorGUILayout.Popup("相机操作类型：",(int)t.CamMoveType, new string[] { "平移", "环视", "旋转" });

        switch (t.CamMoveType)
        {
            case CameraMoveAdv.MoveType.move:
                t.TouchX = (CameraMoveAdv.Axis)EditorGUILayout.Popup("左右拖动对应的移动轴：", (int)t.TouchX, aixs);
                t.MaxOffsetX = EditorGUILayout.FloatField(aixs[(int) t.TouchX] + " 移动距离上限", t.MaxOffsetX);
                t.MaxOffsetX = t.MaxOffsetX < 0 ? 0 : t.MaxOffsetX;
                t.MinOffsetX = EditorGUILayout.FloatField(aixs[(int)t.TouchX] + " 移动距离下限", t.MinOffsetX);
                t.MinOffsetX = t.MinOffsetX > 0 ? 0 : t.MinOffsetX;
                t.BoostX = EditorGUILayout.FloatField(aixs[(int)t.TouchX] + " 移动速度", t.BoostX);
                t.BoostX = t.BoostX < 0 ? 0 : t.BoostX;
                EditorGUILayout.Space();
                t.TouchY = (CameraMoveAdv.Axis)EditorGUILayout.Popup("上下拖动对应的移动轴：", (int)t.TouchY, aixs);
                t.MaxOffsetY = EditorGUILayout.FloatField(aixs[(int)t.TouchY] + " 移动距离上限", t.MaxOffsetY);
                t.MaxOffsetY = t.MaxOffsetY < 0 ? 0 : t.MaxOffsetY;
                t.MinOffsetY = EditorGUILayout.FloatField(aixs[(int)t.TouchY] + " 移动距离下限", t.MinOffsetY);
                t.MinOffsetY = t.MinOffsetY > 0 ? 0 : t.MinOffsetY;
                t.BoostY = EditorGUILayout.FloatField(aixs[(int)t.TouchY] + " 移动速度", t.BoostY);
                t.BoostY = t.BoostY < 0 ? 0 : t.BoostY;
                break;
            case CameraMoveAdv.MoveType.lookAround:
                t.MaxOffsetX = EditorGUILayout.FloatField("水平旋转角度上限", t.MaxOffsetX);
                t.MaxOffsetX = Mathf.Clamp(t.MaxOffsetX, 0, 180);
                t.MinOffsetX = EditorGUILayout.FloatField("水平旋转角度下限", t.MinOffsetX);
                t.MinOffsetX = Mathf.Clamp(t.MinOffsetX, -180, 0);
                t.BoostX = EditorGUILayout.FloatField("水平旋转速度", t.BoostX);
                t.BoostX = t.BoostX < 0 ? 0 : t.BoostX;
                EditorGUILayout.Space();
                t.MaxOffsetY = EditorGUILayout.FloatField("垂直旋转角度上限", t.MaxOffsetY);
                t.MaxOffsetY = Mathf.Clamp(t.MaxOffsetY, 0, 180);
                t.MinOffsetY = EditorGUILayout.FloatField("垂直旋转角度下限", t.MinOffsetY);
                t.MinOffsetY = Mathf.Clamp(t.MinOffsetY, -180, 0);
                t.BoostY = EditorGUILayout.FloatField("垂直旋转速度", t.BoostY);
                EditorGUILayout.Space();
                if (t.LookCenter == null)
                {
                    EditorGUILayout.BeginHorizontal();
                    t.LookCenter =
                        (GameObject)EditorGUILayout.ObjectField("环视中心:", t.LookCenter, typeof(GameObject), true);
                    if (GUILayout.Button("创建环视中心"))
                    {
                        GameObject temp = new GameObject();
                        temp.name = "LOOK_CENTER";
                        t.LookCenter = temp;
                    }
                    EditorGUILayout.EndHorizontal();
                }
                else
                {
                    t.LookCenter =
                        (GameObject)EditorGUILayout.ObjectField("环视中心:", t.LookCenter, typeof(GameObject), true);
                }
               
                break;
            case CameraMoveAdv.MoveType.rotate:
                t.MaxOffsetX = EditorGUILayout.FloatField("水平旋转角度上限", t.MaxOffsetX);
                t.MaxOffsetX = Mathf.Clamp(t.MaxOffsetX, 0, 180);
                t.MinOffsetX = EditorGUILayout.FloatField("水平旋转角度下限", t.MinOffsetX);
                t.MinOffsetX = Mathf.Clamp(t.MinOffsetX, -180, 0);
                t.BoostX = EditorGUILayout.FloatField("水平旋转速度", t.BoostX);
                t.BoostX = t.BoostX < 0 ? 0 : t.BoostX;
                EditorGUILayout.Space();
                t.MaxOffsetY = EditorGUILayout.FloatField("垂直旋转角度上限", t.MaxOffsetY);
                t.MaxOffsetY = Mathf.Clamp(t.MaxOffsetY, 0, 180);
                t.MinOffsetY = EditorGUILayout.FloatField("垂直旋转角度下限", t.MinOffsetY);
                t.MinOffsetY = Mathf.Clamp(t.MinOffsetY, -180, 0);
                t.BoostY = EditorGUILayout.FloatField("垂直旋转速度", t.BoostY);
                break;
        }
        EditorGUILayout.Space();
        if (t.CamMoveType != CameraMoveAdv.MoveType.move ||
            ((t.TouchX != CameraMoveAdv.Axis.localZ && t.TouchY != CameraMoveAdv.Axis.localZ)))
        {
            t.MaxZoom = EditorGUILayout.FloatField("缩放距离上限", t.MaxZoom);
            t.MaxZoom = t.MaxZoom < 0 ? 0 : t.MaxZoom;
            t.MinZoom = EditorGUILayout.FloatField("缩放距离下限", t.MinZoom);
            t.MinZoom = t.MinZoom > 0 ? 0 : t.MinZoom;
            t.BoostZoom = EditorGUILayout.FloatField("缩放速度", t.BoostZoom);
            t.BoostZoom = t.BoostZoom < 0 ? 0 : t.BoostZoom;
        }

        if (EditorGUI.EndChangeCheck())
        {
            //TODO:DELETE
            Debug.Log("####");

            EditorSceneManager.MarkSceneDirty(t.gameObject.scene);
        }
    }
}
