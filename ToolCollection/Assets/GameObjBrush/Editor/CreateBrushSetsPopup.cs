﻿//*****************************************************************************
//Created By ZJ on 2018年12月28日.
//
//@Description 添加画刷集弹窗
//*****************************************************************************
using UnityEditor;
using UnityEngine;

namespace JZYX.GameObjectBrush
{
    public class CreateBrushSetsPopup : EditorWindow
    {
        private const string SAVE_PATH_KEY = "GameObjBrush_SavePath_Key";
        [SerializeField] private string _savePath;
        [SerializeField] private string _saveName = "New Brush Sets";
        private Color _redColor = new Color(1, 0.45f, 0.45f);
        private Color _greenColor = new Color(0.47f, 1f, 0.45f);


        public static void OpenWindow()
        {
            EditorWindow win = GetWindow<CreateBrushSetsPopup>();
            win.maxSize = new Vector2(300, 100);
            win.minSize = new Vector2(300, 100);
            win.Show();
        }

        public void OnEnable()
        {
            if (EditorPrefs.HasKey(SAVE_PATH_KEY))
            {
                _savePath = EditorPrefs.GetString(SAVE_PATH_KEY);
            }
            else
            {
                _savePath = "Brushs";
            }

            Undo.undoRedoPerformed += Repaint;
        }

        public void OnDisable()
        {
            Undo.undoRedoPerformed -= Repaint;
            EditorPrefs.SetString(SAVE_PATH_KEY, _savePath);
        }

        public void OnGUI()
        {
            Undo.RecordObject(this, "Changed Settings");
            GUILayout.Label("新建一个画笔集合");
            GUILayout.BeginHorizontal();
            GUILayout.Label("存储路径:" + "Assets/");
            _savePath = GUILayout.TextField(_savePath, GUILayout.Width(180));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("文件名称(不带后缀):");
            _saveName = GUILayout.TextField(_saveName);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUI.backgroundColor = _redColor;
            if (GUILayout.Button("取消"))
            {
                this.Close();
            }

            GUI.backgroundColor = _greenColor;
            if (GUILayout.Button("创建"))
            {
                if (!AssetDatabase.IsValidFolder(_savePath))
                {
                    CrateDir(_savePath);
                }

                BrushSets b = BrushSets.CreateBrushSets("Assets/" + _savePath + "/" + _saveName + ".asset");
                GameObjBrushEditor.SetBrushSetsIdx(b.GetIndex());
                this.Close();
            }

            GUILayout.EndHorizontal();
        }

        private void CrateDir(string dir)
        {
            string[] pathL = dir.Split('/');
            string path = "Assets";
            foreach (string s in pathL)
            {
                string nowPath = path + "/" + s;

                if (!AssetDatabase.IsValidFolder(nowPath))
                {
                    AssetDatabase.CreateFolder(path, s);
                }

                path = nowPath;
            }
        }
    }
}