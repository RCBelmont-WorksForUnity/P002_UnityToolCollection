﻿//*****************************************************************************
//Created By ZJ on 2018年12月28日.
//
//@Description 添加画刷图层弹窗
//*****************************************************************************
using UnityEditor;
using UnityEngine;

namespace JZYX.GameObjectBrush
{
    public class LayerAddPopup : EditorWindow
    {
        private string _tempTag;
        private Color _redColor = new Color(1, 0.45f, 0.45f);
        private Color _greenColor = new Color(0.47f, 1f, 0.45f);

        public static void OpenWin()
        {
            EditorWindow win = GetWindow<LayerAddPopup>();
            win.maxSize = new Vector2(300, 100);
            win.minSize = new Vector2(300, 100);
            win.Show();
        }

        public void OnGUI()
        {
            GUILayout.Label("新建一个画刷图层");
            GUILayout.BeginHorizontal();
            GUILayout.Label("选择一个Tag: ");
            _tempTag = EditorGUILayout.TagField(_tempTag);
            GUILayout.EndHorizontal();
            GUI.backgroundColor = _redColor;
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("取消"))
            {
                this.Close();
            }

            GUI.backgroundColor = _greenColor;
            if (GUILayout.Button("确定"))
            {
                if (!string.IsNullOrEmpty(_tempTag))
                {
                    GameObjBrushEditor.AddLayer(_tempTag);
                    this.Close();
                }
            }

            EditorGUILayout.EndHorizontal();

        }
    }
}