﻿//*****************************************************************************
//Created By ZJ on 2018年12月27日.
//
//@Description 画刷对象
//*****************************************************************************
using UnityEngine;

namespace JZYX.GameObjectBrush
{
    [System.Serializable]
    public class BrushObj
    {
        //对齐轴方向
        public enum AlignAxisEnum
        {
            X,
            Y,
            Z
        }


        [SerializeField] public GameObject BaseObj; //管理的基础预制体
        [SerializeField] public bool UseClone = false; //是否生成clone对象
        [SerializeField] public float Size = 0.5f; //画刷大小
        [SerializeField] public float Density = 5; //画刷密度
        [SerializeField] public float MinSacleRange = 1; //缩放范围上限
        [SerializeField] public float MaxSacleRange = 10; //缩放范围下限
        [SerializeField] public float MinSacle = 0.5f; //缩放下限
        [SerializeField] public float MaxSacle = 1.5f; //缩放上限

        [SerializeField] public bool CanXRotate = false; //是否启用X轴旋转
        [SerializeField] public float MinXRotate = 0; //最小旋转角度
        [SerializeField] public float MaxXRotate = 180; //最大旋转角度

        [SerializeField] public bool CanYRotate = false; //是否启用Y轴旋转
        [SerializeField] public float MinYRotate = 0; //最小旋转角度
        [SerializeField] public float MaxYRotate = 180; //最大旋转角度

        [SerializeField] public bool CanZRotate = false; //是否启用Z轴旋转
        [SerializeField] public float MinZRotate = 0; //最小旋转角度
        [SerializeField] public float MaxZRotate = 180; //最大旋转角度
        [SerializeField] public bool AlignSurface = true; //是否对齐表面法线
        [SerializeField] public AlignAxisEnum AlignAxis = AlignAxisEnum.Y; //对齐轴

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="baseObjIn"></param>
        public BrushObj(GameObject baseObjIn)
        {
            BaseObj = baseObjIn;
        }
    }
}