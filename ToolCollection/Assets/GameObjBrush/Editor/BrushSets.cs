﻿//*****************************************************************************
//Created By ZJ on 2018年12月27日.
//
//@Description 画刷集
//*****************************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace JZYX.GameObjectBrush
{
    public class BrushSets : ScriptableObject
    {
        [SerializeField] private List<BrushObj> _brushList;
        [SerializeField] private List<string> _tagL;

        [SerializeField] public int SelectBrushLayer = 0;

        //图层列表
        public List<string> TagL
        {
            get
            {
                if (_tagL == null)
                {
                    _tagL = new List<string>();
                    _tagL.Add("DefualtLayer");
                }

                return _tagL;
            }
            set
            {
                if (value == null)
                {
                    value = new List<string>();
                    _tagL.Add("DefualtLayer");
                }

                _tagL = value;
            }
        }

        //画刷列表
        public List<BrushObj> BrushList
        {
            get
            {
                if (_brushList == null)
                {
                    _brushList = new List<BrushObj>();
                }

                return _brushList;
            }
            set
            {
                if (value == null)
                {
                    value = new List<BrushObj>();
                }

                _brushList = value;
            }
        }

        //收集所有画刷集资源的路径
        public static string[] GetAllBrushSetsGUIDs()
        {
            return AssetDatabase.FindAssets("t:BrushSets");
        }

        //获取画刷集列表
        public static BrushSetsList GetAllBrushSets()
        {

            return new BrushSetsList(GetAllBrushSetsGUIDs());
        }

        /// <summary>
        /// 添加画刷图层
        /// </summary>
        /// <param name="tag"></param>
        public void AddLayer(string tag)
        {
            if (!TagL.Contains(tag))
            {
                TagL.Add(tag);
                Save();
            }
        }

        /// <summary>
        /// 获取画刷集GUID
        /// </summary>
        /// <returns></returns>
        public string GetGUID()
        {
            return AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(this));
        }

        /// <summary>
        /// 保存画刷集数据
        /// </summary>
        public void Save()
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }

        /// <summary>
        /// 添加画刷
        /// </summary>
        /// <param name="prefb"></param>
        public void AddBrush(GameObject prefb)
        {
            if (prefb == null)
            {
                return;
            }

            BrushObj obj = new BrushObj(prefb);
            this._brushList.Add(obj);
        }

        /// <summary>
        /// 获取画刷集索引
        /// </summary>
        /// <returns></returns>
        public int GetIndex()
        {
            string guid = GetGUID();
            string[] guidL = GetAllBrushSetsGUIDs();
            for (int i = 0; i < guidL.Length; i++)
            {
                if (guidL[i] == guid)
                {
                    return i;
                }
            }

            return 0;
        }

        /// <summary>
        /// 获取当前画刷图层
        /// </summary>
        /// <returns></returns>
        public string GetNowLayer()
        {
            string tag = TagL[SelectBrushLayer];
            if (!InternalEditorUtility.tags.Contains(tag))
            {
                InternalEditorUtility.AddTag(tag);
            }

            return TagL[SelectBrushLayer];
        }

        /// <summary>
        /// 移除画刷
        /// </summary>
        /// <param name="targetBrushL"></param>
        public void RemoveBrush(List<BrushObj> targetBrushL)
        {
            foreach (BrushObj brush in targetBrushL)
            {
                if (_brushList.Contains(brush))
                {
                    _brushList.Remove(brush);
                }
            }

            Save();
        }

        /// <summary>
        /// 移除所有画刷
        /// </summary>
        public void RemoveAllBrushs()
        {
            _brushList.Clear();
            Save();
        }

        /// <summary>
        /// 创建画刷集
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static BrushSets CreateBrushSets(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                Debug.LogError("笔刷资源路径为空");
                return null;
            }

            BrushSets bs = CreateInstance<BrushSets>();
            AssetDatabase.CreateAsset(bs, path);
            AssetDatabase.SaveAssets();
            return bs;
        }

        /// <summary>
        /// 画刷列表
        /// </summary>
        public struct BrushSetsList
        {
            public List<BrushSets> BrushSetsL;

            /// <summary>
            /// 构造函数
            /// </summary>
            /// <param name="guids"></param>
            public BrushSetsList(string[] guids)
            {
                BrushSetsL = new List<BrushSets>();
                foreach (string guid in guids)
                {
                    string path = AssetDatabase.GUIDToAssetPath(guid);
                    BrushSetsL.Add(AssetDatabase.LoadAssetAtPath<BrushSets>(path));
                }
            }

            /// <summary>
            /// 获取画刷集名称列表
            /// </summary>
            /// <returns></returns>
            public string[] GetNameL() 
            {
                List<string> ret = new List<string>();
                foreach (BrushSets brushSets in BrushSetsL)
                {

                    if (ret.Contains(brushSets.name))
                    {
                        string path = AssetDatabase.GetAssetPath(brushSets);

                        ret.Add(path.Replace("/", @"\"));
                        continue;
                    }

                    ret.Add(brushSets.name);
                }

                return ret.ToArray();
            }
        }
    }
}